use rstest::rstest;
use url::Url;
 
#[rstest]
// domain removed #[case("https://netmag.ml/index.xml")]
// domain removed #[case::rome_tools("https://rome.tools/feed.xml")]
#[case::week_in_rust("https://this-week-in-rust.org/rss.xml")]
fn parse_real_feed(#[case] uri: Url) {
   let request = indexmenow::rss::getFeed(&uri);
   assert!(request.is_ok(), "Feed parsing of {} failed: {:?}", uri, request);
   let feed = request.unwrap();
   assert!( feed.len() > 0 );
}
