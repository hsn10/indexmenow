# Index Me Now

A command-line utility for reading [RSS feeds](https://www.rssboard.org/rss-specification) 
and sending them to search engines that supports the 
[IndexNow protocol](https://www.indexnow.org/) for faster updates.

[Atom](https://www.rfc-editor.org/rfc/rfc4287) syndication format is *not supported*. It is planned for next
release if I found not bloated library supporting both RSS and Atom.

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://spdx.org/licenses/MIT.html)
[![Crates.io](https://img.shields.io/crates/v/indexmenow.svg)](https://crates.io/crates/indexmenow)
[![Crates.io MSRV](https://img.shields.io/crates/msrv/indexmenow?logo=rust&label=MSRV&labelColor=orange)](https://blog.rust-lang.org/2022/09/22/Rust-1.64.0.html)
[![Safe Rust](https://img.shields.io/badge/Rust-safe-brightgreen.svg)](https://www.rust-lang.org/policies/safety.html)
[![dependency status](https://deps.rs/repo/gitlab/hsn10/indexmenow/status.svg)](https://deps.rs/repo/gitlab/hsn10/indexmenow)
[![Documentation](https://docs.rs/indexmenow/badge.svg)](https://docs.rs/indexmenow)
[![](https://tokei.rs/b1/gitlab/hsn10/indexmenow?category=code)](https://github.com/XAMPPRocky/tokei)
[![Downloads](https://img.shields.io/crates/d/indexmenow)](https://crates.io/crates/indexmenow/versions)

## Configuration
Program uses configuration file *indexmenow.toml*. File is in [TOML syntax](https://toml.io/en/v1.0.0).

You configure there service endpoints and RSS feeds.
*site* and *endpoint* sections can be repeated.

### RSS feed configuration
```
[[site]]
name = "NetMag"
host = "netmag.ml"
rss  = "https://netmag.ml/index.xml"
key  = "86242457893124524677323557"
keyLocation = "https://netmag.ml/86242457893124524677323557.txt"
```

### IndexNow endpoint configuration

```
[[endpoint]]
name = "Yandex"
url  = "https://yandex.com/indexnow"
```