//! Command line utility for sending IndexNow request to search engine
//! based on RSS feed.
//!
//! See homepage for more information:
//! https://gitlab.com/hsn10/indexmenow

#![forbid(unsafe_code)]

#[cfg(feature = "enable-tests-common")]
pub mod rss;
