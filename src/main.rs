#![forbid(unsafe_code)]
#![allow(unused_parens)]

/**
  IndexNow client CLI
*/

mod siteconfig;
mod rss;
mod indexnow;

const SITE_CONFIG: &str = "indexmenow.toml";

fn main() {
    match siteconfig::readToml(SITE_CONFIG) {
       Ok(sc) => indexnow::announceFeeds(sc),
       Err(e) => println!("Failed to read config file {} error: {}", SITE_CONFIG, e)
    }  
}
