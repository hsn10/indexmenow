#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use serde_derive::Deserialize;
use url::Url;

/**
 * Entire parsed TOML config
*/
#[derive(Deserialize,Debug,Clone)]
pub(crate) struct tomlConfig {
   #[serde(rename = "site")]
   pub sites: Vec<siteConfig>,
   #[serde(rename = "endpoint")]
   pub endpoints: Vec<endPointConfig>
}

/**
 * Site configuration
*/
#[derive(Deserialize,Debug,Clone)]
pub(crate) struct siteConfig {
   pub name: Option<String>,
   pub host: String,
   pub key:  String,
   pub keyLocation: Option <Url>,
   pub rss:  Option <Url>
}

/**
 * IndexNow endpoint configuration
*/
#[derive(Deserialize,Debug,Clone)]
pub(crate) struct endPointConfig {
   pub name: String,
   pub url:  Url,
}

/**
 * Read toml configuration file.
*/
pub(crate) fn readToml(filename: impl AsRef<str>) -> Result<tomlConfig,Box<dyn std::error::Error>> {
    match (std::fs::read_to_string(filename.as_ref())) {
       Ok(config) => {
          use serde::de::IntoDeserializer;
          match config.parse::<toml::Table>() {
             Ok(toml) =>
                match <tomlConfig as serde::Deserialize>::deserialize(toml.into_deserializer()) {
                   Ok(siteconfig) => Ok(siteconfig),
                   Err(e) => Err(Box::new(e))
                }
             Err(e) => Err(Box::new(e))
          }
       },
       Err(e) => Err(Box::new(e))
    }
}