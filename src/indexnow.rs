#![allow(non_snake_case)]

use url::Url;
use serde_derive::Serialize;

pub(crate) fn announceFeeds(sites: crate::siteconfig::tomlConfig) {
   for site in sites.sites {
      if let Some(uri) = &site.rss {
         print!("Loading feed for {}", nameOrHost(&site));
         flushTerm();
         if let Ok(items) = crate::rss::getFeed(&uri) {
            println!(".");
            // announce first 2 items
            let toAnnounce : Vec<Url> = items.iter().map(|item| item.link.clone()).take(2).collect();
            for target in &sites.endpoints {
               // construct payload
               let p = IndexNowRequest {
                  host: String::from(&site.host),
                  key:  String::from(&site.key),
                  keyLocation: site.keyLocation.clone(),
                  urlList: toAnnounce.clone()
               };
               print!("  .. submiting to {}", &target.name);
               flushTerm();
               if let Ok(_) = sendIndexNow(&target.url, p) {
                  println!(".");
               } else {
                  println!(" .. failed");
               }
            }
         } else {
            // feed loading failed
            println!(" .. failed");
         }
      }
   }
}

/**
 * Returns site name or host name
*/
fn nameOrHost(site: &crate::siteconfig::siteConfig) -> String {
   site.name.as_ref().unwrap_or(&site.host).to_string()
}

/**
 * Flushes stdout.
 *
 * Possible error is ignored.
*/
fn flushTerm() {
   use std::io::Write;
   let _ = std::io::stdout().flush();
}

/*
{
  "host": "www.example.org",
  "key": "726a90b3ec6c44e2aa22289104a088ed",
  "keyLocation": "https://www.example.org/726a90b3ec6c44e2aa22289104a088ed.txt",
  "urlList": [
      "https://www.example.org/url1",
      "https://www.example.org/folder/url2",
      "https://www.example.org/url3"
      ]
}
*/

#[derive(Serialize,Debug,Clone)]
pub(crate) struct IndexNowRequest {
   pub host:        String,
   pub key:         String,
   pub keyLocation: Option<Url>,
   pub urlList:     Vec<Url>
}

/**
  Send request to search engine endpoint.

  https://www.bing.com/indexnow
*/
fn sendIndexNow(endpoint: &Url, payload: IndexNowRequest) -> Result<(),Box<dyn std::error::Error>> {
   // serialize to json
   let json = serde_json::to_string(&payload)?;
   let request = minreq::post(endpoint.to_string())
       .with_body(json);
   let response = request.send()?;
   if ( response.status_code != 200 ) {
      Err(Box::new(std::io::Error::new(std::io::ErrorKind::InvalidData, "Invalid HTTP return code")))
   } else {
      Ok(())
   }
}