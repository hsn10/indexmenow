#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused_parens)]

use rss::Channel;

#[derive(Debug,Clone)]
pub struct feedItem {
   pub link: url::Url,
   pub pub_date: Option<String>
}

const TIMEOUT : u64 = 30;

pub fn getFeed(uri: &url::Url) -> Result<Vec<feedItem>,Box<dyn std::error::Error>> {
   if let Ok(r) = minreq::get(uri.as_str()).with_timeout(TIMEOUT).send() {
      if ( r.status_code == 200 ) {
            // feed body read
            match (Channel::read_from(r.as_bytes())) {
               Ok(channel) => {
                  // get items out
                  let mut rc: Vec<feedItem> = Vec::new();
                  for i in channel.items() {
                     if let Some(urlstr) = &i.link {
                        if let Ok(u) = url::Url::parse(&urlstr) {
                           rc.push( feedItem { link: u, pub_date: i.pub_date.clone() } );
                        }
                     }
                  }
                  Ok(rc)
               },
               Err(e) => Err(Box::new(e))
            }
      } else {
         Err(Box::new(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, "Invalid HTTP return code")))
      }
   } else {
      Err(Box::new(std::io::Error::new(std::io::ErrorKind::NotConnected, "connection failed")))
   }
}

#[cfg(test)]
#[ path = "." ]
mod tests {
   #[path = "feeditem_tests.rs"]
   mod feedItem;
}
