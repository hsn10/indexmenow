use super::super::*;
use rstest::{rstest,fixture};

/**
 * If this test compiles fields have Option type
*/
#[rstest]
fn member_pubdate_is_Option_field(testUrl: url::Url) {
   feedItem {
              link: testUrl,
              pub_date: None
   };
}

/**
 * Create sample Url object for use in tests
*/
#[fixture]
fn testUrl() -> url::Url {
   const TEST: &str = "https://www.example.com";
   let uri = url::Url::parse(TEST);
   assert!( uri.is_ok() );
   uri.unwrap()
}

/**
 * Check if link is Url type
*/
#[rstest]
fn member_link_is_Url_field(testUrl: url::Url) {
   feedItem {
              link: testUrl,
              pub_date: None
   };
}